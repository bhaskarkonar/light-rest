package com.swissre.lite.rest.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.swissre.lite.rest.model.News;

@Path("/news")
public class NewsService {
	
	@GET
	@Path("/recent")
	@Produces(MediaType.APPLICATION_JSON)
	public List<News> getRecentNews(){
		List<News> recentNews=new ArrayList<News>();
		SimpleDateFormat ft = 
			      new SimpleDateFormat ("dd-MM-yyyy");
		for(int i=1; i<=10;i++){
			
			News news=new News();
			news.setTitle("Title "+i);
			news.setSource("Source "+i);
			news.setContent("Content "+i);
			news.setUrl("http://www.google.com");
			news.setDate(ft.format(new Date()));
			
			recentNews.add(news);
		}

		return recentNews;
	}

}
